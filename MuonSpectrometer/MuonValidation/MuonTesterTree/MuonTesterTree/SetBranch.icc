/*
  Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/
#ifndef MUONTESTER_MUONSSetBranch_IXX
#define MUONTESTER_MUONSSetBranch_IXX

#include "MuonTesterTree/throwExcept.h"
namespace MuonVal {
template <class T> SetBranch<T>::SetBranch(TTree* tree, const std::string& name) : MuonTesterBranch(tree, name) {}

template <class T> SetBranch<T>::SetBranch(MuonTesterTree& tree, const std::string& name) : MuonTesterBranch(tree, name) {}

template <class T> bool SetBranch<T>::fill(const EventContext&) {
    if (!initialized()) {
        ATH_MSG_ERROR("fill()  -- The  set branch " << name() << " is not initialized yet.");
        return false;
    }
    if (!m_updated) m_variable.clear();
    m_updated = false;
    return true;
}
template <class T> bool SetBranch<T>::init() { return addToTree(m_variable); }
template <class T> size_t SetBranch<T>::size() const {
    if (!m_updated) return 0;
    return m_variable.size();
}
template <class T> std::set<T>& SetBranch<T>::operator->(){ return get();}
template <class T> std::set<T>& SetBranch<T>::get() {
    if(!m_updated) m_variable.clear();
    m_updated = true;
    return m_variable;
}
template <class T> void SetBranch<T>::operator=(const std::set<T>& other) {
    get() = other;    
}
template <class T> const std::set<T>& SetBranch<T>::operator->() const{ return get();}
template <class T> const std::set<T>& SetBranch<T>::get() const { 
    if (!m_updated) {
        static const std::set<T> dummy{};
        return dummy;
    }
    return m_variable;
}
template <class T> void SetBranch<T>::insert(const T& value) {
    if (!m_updated) m_variable.clear();
    // Throw an exception in cases where the branch is not initialized
    if (!initialized()) { THROW_EXCEPTION(name() + " is not initialized yet"); }
    m_variable.insert(value);
    m_updated = true;
}
template <class T> void SetBranch<T>::operator+=(const T& value) { insert(value); }
template <class T> bool SetBranch<T>::isUpdated() const { return m_updated; }
}
#endif